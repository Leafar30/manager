import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import firebase from 'firebase';
import reducers from './src/reducers';
import Router from './src/Router';

console.ignoredYellowBox = ['Setting a timer'];

class App extends Component {
   componentWillMount() {
    const config = {
      apiKey: 'AIzaSyDzPdyWAh_7qCZZO4bIljqn_KT-5FKLA-o',
      authDomain: 'manager-5b942.firebaseapp.com',
      databaseURL: 'https://manager-5b942.firebaseio.com',
      projectId: 'manager-5b942',
      storageBucket: 'manager-5b942.appspot.com',
      messagingSenderId: '235582765347'
    };

    firebase.initializeApp(config);
   }
  render() { 
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

     return (
       <Provider store={store}>
        <Router />
       </Provider> 
     );
  }
}

export default App;


