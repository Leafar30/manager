//lodash is a library to help to transform objects in array
import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { employeeFetch } from '../actions';
import { ListView } from 'react-native';
import ListItem from './ListItem';

class EmployeeList extends Component {

    //lifecycle method 
    componentWillMount(){
      this.props.employeeFetch();  //Async Operation

      this.createDataSource(this.props);     
    };
    //to get the full data from firebase
    componentWillReceiveProps(nextProps) {
    //nextProps are the next set of props that this component 
    //will be rendered with
    //this.props is still the old sset of props
    this.createDataSource(nextProps); 
    };

    createDataSource({ employees }) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1,r2) => r1 !== r2 
         });
        this.dataSource = ds.cloneWithRows(employees);
    };

    renderRow(employee) {
        return <ListItem employee={employee} />;
    };
   render() {
        return(
            <ListView 
            enableEmptySections
            dataSource={this.dataSource}
            renderRow={this.renderRow}
            />
       );
   }

}

const mapStateToProps = (state) => {
  //_.map (foreach  - key value pair)  
  //run the federal funtion (val,uid) =>....
  //val = contains the name,phone and shift properties
  //uid = id of the record "those long weird letters and numbers"
   const employees = _.map(state.employees, (val, uid) => {
  // ...val = create a new object pushing the values  
  // key value = Only because I am using FlatLst instead of ListView 
       return { ...val, uid }; //end result {shift: 'Monday', name: 'SS' }
   });
       return { employees };
};

export default connect (mapStateToProps, { employeeFetch })(EmployeeList);