import React, { Component } from 'react';
import EmployeeForm from './EmployeeForm';
import _ from 'lodash';
import { connect } from 'react-redux';
import  Communications  from 'react-native-communications';
//employeeUpdate changes the states of the reducer, hence 
//the library here to be exported.
import { employeeUpdate, employeeSave } from '../actions';
import { Card, CardSection, Button } from './common';


class EmployeeEdit extends Component {
    componentWillMount() {
    //interating over all the properties of the employee
    // _.each (foreach loop)    
     _.each(this.props.employee, (value, prop) => {
    // updating the reducer below...
           this.props.employeeUpdate({ prop, value });
     });

    }

    onButtonPress(){
     const { name, phone, shift } = this.props;
     // uid: this.props."employee".uid - employee coming from ListItem component
     // under Actions.employeeEdit({ employee: this.props.employee });
     this.props.employeeSave({ name, phone, shift, uid: this.props.employee.uid });
    };

    onTextPress() {
       const { phone, shift } = this.props;
     Communications.text(phone, `Hey Biatch come to work on ${shift}`);
    };
   // Unmount clear the data left from the reducer to create a new user.
   // Add Employee form is clear now
    componentWillUnMount() {
        const clearData = {
            name: '',
            phone: '',
            shift: 'Monday'
        };
        _.each(clearData, (value, prop) => {
            this.props.employeeUpdate({ prop, value });
          });        
  };
    render() {
      return(
       <Card>
        <EmployeeForm  /> 

         <CardSection>
             <Button onPress={this.onButtonPress.bind(this)} >
              Save Changes   
             </Button>   
         </CardSection> 

         <CardSection>
             <Button onPress={this.onTextPress.bind(this)} >
              Text Message   
             </Button> 
         </CardSection>    
       </Card>    

      );
    
    };
};

mapStateToProps = (state) => {
   const { name, phone, shift } = state.employeeForm;

   return { name, phone, shift };
};

export default connect(mapStateToProps, { employeeUpdate, employeeSave }) (EmployeeEdit);