import React from 'react';
import { View } from 'react-native';

const CardSection = (props) => {
    return (
        //the square brackets means you can modify your CarSection
        // style in any component without edit here (it makes more generic)
        <View style={[styles.containerStyle, props.style]}>
        {props.children}
        </View> 
    );
};

const styles = {
    containerStyle: {
            borderBottomWidth: 1,
            padding: 5,
            backgroundColor: '#fff',
            justifyContent: 'flex-start',
            flexDirection: 'row',
            borderColor: '#ddd',
            position: 'relative',
    }
};

export { CardSection };