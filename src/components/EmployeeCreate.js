import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { employeeUpdate, employeeCreate } from '../actions';
import { Card, CardSection, Button } from './common';
import EmployeeForm from './EmployeeForm';


class EmployeeCreate extends Component {
   // this reset the form after editting occurs.
    componentWillMount() {
        const clearData = {
          name: '',
          phone: '',
          shift: 'Monday'
        };
        _.each(clearData, (value, prop) => {
          this.props.employeeUpdate({ prop, value });
        });
      };

    onButtonPress() {
        const { name, phone, shift } = this.props;
        this.props.employeeCreate({ name, phone, shift: shift || 'Monday' });
    }
    render() {
      // EmployeeForm {...this.props } means passing everything from
      // EmployeeCreate form to EmployeeForm form..
    return(
     <Card>   
         <EmployeeForm {...this.props} />
          <CardSection>
             <Button onPress={this.onButtonPress.bind(this)}>
                 Create
             </Button>    
         </CardSection>


     </Card>

    );
    }
}


const mapStateToProps = (state) => {
  const {
      name,
      phone,
      shift
  } = state.employeeForm;

  return { name, phone, shift  };

};

export default connect(mapStateToProps, { employeeUpdate, employeeCreate }) (EmployeeCreate);