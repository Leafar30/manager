import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';
import { EMPLOYEE_UPDATE,
  EMPLOYEE_CREATE, 
  EMPLOYEE_SUCCESS_FETCH,
  EMPLOYEE_SAVE_SUCCESS } from "./types";


export const employeeUpdate = ({ prop, value }) => {
   return {
        type: EMPLOYEE_UPDATE,
        payload: { prop, value }
}; 
  
};

export const employeeCreate = ({ name, phone, shift }) => {
// currentUser linked with firebase authentication       
        const { currentUser } = firebase.auth();

// 

  return (dispatch) =>  {
       firebase.database().ref(`/users/${ currentUser.uid }/employees`)
         .push({ name, phone, shift })
         // Actions.pop takes care to take back to previous scene!
         .then(() => 
         dispatch({ type: EMPLOYEE_CREATE }),
         Actions.employeeList({ type: 'reset' }));
    };     
       
 };     

//using redux thunk 
//getting information from firebase to bring to the application.
export const employeeFetch = () => {
    const { currentUser } = firebase.auth();  
   
   
        return (dispatch) => {
          firebase.database().ref(`/users/${currentUser.uid}/employees`) 
            .on('value', snapshot => {
           //  Snapshot.val() thats how you get access to data 
                dispatch({type: EMPLOYEE_SUCCESS_FETCH, payload: snapshot.val()});
            });
       };
};

export const employeeSave = ({ name, phone, shift, uid }) => {
     const  { currentUser } = firebase.auth();
     
     return (dispatch) => {
      //${uid} - to get the specific user to update.
      firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
      .set({ name, phone, shift })
      // Actions.pop takes care to take back to previous scene!
      .then(() => 
      dispatch({ type: EMPLOYEE_SAVE_SUCCESS }),
      Actions.employeeList({ type: 'reset' }));
     };

};